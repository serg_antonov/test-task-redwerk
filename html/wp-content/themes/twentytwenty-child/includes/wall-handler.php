<?php
    class Wall {

        /**
         * WPDB object
         *
         * @var object
         */
        protected $db = null;

        /**
         * Table name
         *
         * @var string
         */
        protected $table = null;

        public function __construct() {
            global $wpdb;

            $this->db = $wpdb;
            $this->table = $this->db->prefix.'wall';

            $this->init_table();
        }

        public function add_message(string $message, int $parent=0) {
            if( (bool) wp_get_current_user()->ID ) {
                $insert_array = array(
                    'author_id' => get_current_user_id(),
                    'comment_parent' => intval($parent),
                    'comment_text' => htmlentities($message, ENT_QUOTES|ENT_DISALLOWED),
                );
                $types_array = array(
                    '%d',
                    '%d',
                    '%s',
                );
                if($this->db->insert($this->table, $insert_array, $types_array)){
                    // message added. send email notification
                    if($parent != 0) {
                        $parent_message = $this->db->get_results( 'SELECT * FROM `'.$this->table.'` WHERE `comment_parent`="'.intval($parent).'"' );
                        
                        $user = get_userdata($parent_message->author_id)->user_email;
                        $subject = __('You have answer to your message', 'twentytwenty-child');
                        $email_message = __('You have answer to your message', 'twentytwenty-child');;

                        wp_mail($user, $subject, $email_message);
                    }
                }
            }
        }

        /**
         * Get all messages as HTML
         *
         * @TODO Need find ways for optimize this method. This is overload to database
         * @param integer $parent_id Parent of messages
         * @return void
         */
        public function get_messages(int $parent_id=0) {
            $messages = $this->db->get_results( 'SELECT * FROM `'.$this->table.'` WHERE `comment_parent`="'.$parent_id.'"' );
            $result = '';
            foreach ($messages as $message) {
                $result .= '<div class="message" style="padding-left: 50px">';
                $result .= '<div class="head">
                    <span class="author">'.get_userdata($message->author_id)->user_login.'</span>
                    <span class="reply"><a href="#'.$message->comment_id.'">reply</a></span>
                </div>';
                $result .= '<div class="text">'.$message->comment_text.'</div>';
                $child_messages = $this->get_messages($message->comment_id);
                if( !empty($child_messages) ) {
                    $result .= $child_messages;
                }
                $result .= '</div>';
            }
            return $result;
        }

        public function __destruct() {
            unset($db);
        }

        /**
         * Install table if this table not exists
         *
         * @return void
         */
        protected function init_table() {
            $sql = '
            CREATE TABLE IF NOT EXISTS '.$this->table.'(
                `comment_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                `author_id` BIGINT(255) UNSIGNED NOT NULL,
                `comment_parent` INTEGER UNSIGNED,
                `comment_text` TEXT)
            ';
            $this->db->query($sql);
        }
    }
?>