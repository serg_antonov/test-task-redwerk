<?php
/**
* Template Name: Wall
*/
?>
<?php
    wp_register_script('wall-js', get_stylesheet_directory_uri().'/assets/js/wall.js', ['jquery'], false, true);
    wp_enqueue_script('wall-js');
    
    get_template_part('header');
    $messages = '';
    $wall_handler = __DIR__.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'wall-handler.php';
    if( file_exists($wall_handler) ) {
        require_once($wall_handler);

        $wall = new \Wall();
        if( (bool) wp_get_current_user()->ID ) {
            if( array_key_exists('wall_message', $_POST) ) {
                if( !empty(trim($_POST['wall_message'])) ) {
                    if(array_key_exists('wall_parent', $_POST)) {
                        $parent = intval($_POST['wall_parent']);
                    } else {
                        $parent = 0;
                    }
                    $wall->add_message($_POST['wall_message'], $parent);
                }
            }

            ob_start();
            ?>
            <div class="messages">
                <?php echo($wall->get_messages()); ?>
            </div>
            <div class="form">
                <form method="post">
                    <textarea name="wall_message" class="message" placeholder="<?php _e('Your message or answer', 'twentytwenty-child'); ?>"></textarea>
                    <input type="hidden" name="wall_parent" />
                    <button type="submit" name="send_message_to_wall"><?php _e('Send', 'twentytwenty-child'); ?></button>
                </form>
            </div>
            <?php
            $wall_html = ob_get_clean();
        } else {
            ob_start();
            ?>
                For access to messages you need sign up to your account.
            <?php
            do_shortcode('simple_login');
            $wall_html = ob_get_clean();
        }
    } else {
        wp_die( __('Internal error, please, try later') );
    }
?>
    <main id="site-content" role="main">
        <article class="post-<?php echo get_the_ID(); ?> page type-page status-publish hentry" id="post-<?php echo get_the_ID(); ?>">
            <header class="entry-header has-text-align-center header-footer-group">
                <div class="entry-header-inner section-inner medium">
                    <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
                </div>
            </header>
            <div class="post-inner thin">
                <div class="entry-content">
                    <div class="simple">
                        <div class="wall-container">
                            <?php echo($wall_html); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>
<?php
    get_template_part('footer');
?>