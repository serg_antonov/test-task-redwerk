jQuery( document ).ready(function() {
    /**
     * TABS
     */
    // Switch to auth tab
    jQuery( "#simple_login_auth_tab" ).click(function() {
        jQuery( "#simple_login_auth" ).toggle( "fast" );
        jQuery( "#simple_login_register" ).toggle( "fast" );
    });

    // Switch to register tab
    jQuery( "#simple_login_register_tab" ).click(function() {
        jQuery( "#simple_login_auth" ).toggle( "fast" );
        jQuery( "#simple_login_register" ).toggle( "fast" );
    });
});