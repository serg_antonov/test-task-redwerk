<?php
/**
* Plugin Name: Simple login & register plugin
* Plugin URI: http://localhost
* Description: Test plugin for login and register user. This plugin generate shortcode.
* Version: 1.0
* Author: Serg Antonov
* Author URI: http://localhost
**/
?>
<?php
    // main JS handler
    wp_register_script('simple-login-main-js', plugin_dir_url(__DIR__).'/simple-login/assets/js/simple-login.js', ['jquery'], null, true);
    wp_enqueue_script('simple-login-main-js');

    // bootstrap
    wp_register_style('simple-login-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', [], null, 'all');
    wp_enqueue_style('simple-login-bootstrap');
    // main css
    wp_register_style('simple-login-main-css', plugin_dir_url(__DIR__).'/simple-login/assets/css/simple-login.css', [], null, 'all');
    wp_enqueue_style('simple-login-main-css');

    /**
     * Generate shortcode for insert auth & register form
     *
     * @return string
     */
    function simple_login_shortcode_generation() : string {

        global $wp;
        $curent_user = get_current_user_id();

        ob_start();

        echo('<div class="simple">');
            if( !(bool)$curent_user ):
                ?>
                    <div id="simple_login_tabs">
                        <ul>
                            <li id="simple_login_auth_tab">Auth</li>
                            <li id="simple_login_register_tab">Register</li>
                        </ul>
                    </div>
                    <form method="post" action="<?php echo home_url(); ?>/wp-login.php" id="simple_login_auth">
                        <div class="form-group">
                            <label>
                                <span>Your login</span>
                                <input type="text" name="log" placeholder="Your login" />
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                <span>Your password</span>
                                <input type="text" name="pwd" placeholder="Your password" />
                            </label>
                        </div>
                        <input type="hidden" name="redirect_to" value="<?php echo home_url( $wp->request ) ?>">
                        <button name="wp-submit">Sign in</button>
                    </form>
                    <form method="post" action="<?php echo home_url(); ?>/wp-login.php?action=register" id="simple_login_register">
                        <div class="form-group">
                                <label>
                                    <span>Your login</span>
                                    <input type="text" name="user_login" placeholder="Your login" />
                                </label>
                        </div>
                        <div class="form-group">
                                <label>
                                    <span>Your e-mail</span>
                                    <input type="text" name="user_email" placeholder="Your e-mail" />
                                </label>
                        </div>
                        <input type="hidden" name="redirect_to" value="<?php echo home_url( $wp->request ) ?>">
                        <button name="wp-submit">Register</button>
                    </form>
                <?php
            else:
                ?>
                    Hello, <b><?php echo get_userdata($curent_user)->user_login ?></b>. You was authorized. <a href="<?php echo home_url(); ?>/wp-login.php?action=logout">Sign out</a>
                <?php
            endif;
        echo('</div>');

        $result = ob_get_clean();

        return $result;
    }
    add_shortcode('simple_login', 'simple_login_shortcode_generation');

    /**
     * Auth user after registration
     *
     * @param int $user_id
     * @return void
     */
    function simple_login_auth_after_registration( $user_id ) {
        global $wp;

        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
        $user = get_user_by( 'id', $user_id );
        do_action( 'wp_login', $user->user_login );
        wp_redirect( home_url( $wp->request ) );
        wp_die();
    }
    add_action( 'user_register', 'simple_login_auth_after_registration' );
?>